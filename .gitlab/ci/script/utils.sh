#!/bin/bash

function success() {
  echo -e "\033[1;32m$1\033[0m"
}

function warn() {
  echo -e "\033[1;33m$1\033[0m"
}

function info() {
  echo -e "\033[1;34m$1\033[0m"
}

function title() {
  echo -e "\033[1;35m** $1 **\033[0m"
}

function log_with_header() {
  length=$(echo "$1" | awk '{print length}')
  delimiter=$(head -c $length </dev/zero | tr '\0' "${2:-=}")

  echo -e "\033[1;35m$delimiter\033[0m"
  echo -e "\033[1;35m$1\033[0m"
  echo -e "\033[1;35m$delimiter\033[0m"
}

function update_dependencies() {
  log_with_header "Update chart dependencies"

  if [ -d "$CHART_DIR/charts" ]; then
    success "Cached dependencies exist, skipping ..."
  else
    helm repo add bitnami https://charts.bitnami.com/bitnami
    helm dependency build $CHART_DIR --skip-refresh
  fi
}
