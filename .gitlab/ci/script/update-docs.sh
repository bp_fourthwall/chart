#!/bin/bash

set -eu

source "$(dirname "$0")/utils.sh"

log_with_header "Updating README.md"

title "Checking if GIT_PUSH_TOKEN is present"
if [ -z "${GIT_PUSH_TOKEN:-}" ]; then
  warn "GIT_PUSH_TOKEN is not present!"
  warn "Please create project access token with write_repository permissions and add GIT_PUSH_TOKEN variable in your GitLab project CI/CD settings"
  exit 1
else
  success "GIT_PUSH_TOKEN is present"
fi

title "Configuring git"

info "Setting up git user"
if [ -z "${GITLAB_USER_EMAIL:-}" ]; then
  warn "GITLAB_USER_EMAIL is not present!"
  warn "Using default email 'ci@example.com'"
  warn "You may need to remove valid user validation under 'Repository > Push rules' settings"
  GITLAB_USER_EMAIL="ci@example.com"
fi
git config --global user.name "CI"
git config --global user.email "$GITLAB_USER_EMAIL"
success "Successfully configured git user"

info "Setting origin to 'gitlab.com/${CI_MERGE_REQUEST_SOURCE_PROJECT_PATH}'"
git remote set-url origin "https://git-push-token:${GIT_PUSH_TOKEN}@gitlab.com/${CI_MERGE_REQUEST_SOURCE_PROJECT_PATH}.git"
success "Successfully set origin"

info "Fetching origin and checking out branch"
git fetch && git checkout $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME
success "Successfully configured git and set current branch to '$CI_MERGE_REQUEST_SOURCE_BRANCH_NAME'"

title "Running helm-docs"
helm-docs -c charts -o ../../README.md
success "Successfully updated README.md"

title "Committing and pushing changes"
if (git diff --quiet && git diff --staged --quiet); then
  warn "No changes detected, skipping ..."
  exit 0
else
  info "Committing changes"
  git add README.md
  git commit -m "Update README.md"

  info "Pushing changes"
  git push origin $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME

  success "Changes committed and pushed"
fi
