#!/bin/bash

set -eo pipefail

source "$(dirname "$0")/utils.sh"

function helm-install() {
  command=$1
  chart=$2
  values="${3:-.gitlab/ci/kube/values/${VALUES}.yaml}"
  override_values=".gitlab/ci/kube/override/${VALUES}.yaml"

  if [[ "$command" != "diff upgrade" ]]; then
    extra_args="--timeout 8m --wait --wait-for-jobs"
  else
    extra_args="--allow-unreleased --show-secrets --color --context 5"
  fi

  if [[ "$command" == "install" && "$CI_JOB_NAME" == "upgrade"* ]] && [[ -f "$override_values" ]]; then
    override_arg="--values ${override_values}"
  fi

  helm $command "$RELEASE_NAME" "$chart" \
    --set env.gitlabUrl="http://gitlab-smocker.gitlab.svc.cluster.local:8080" \
    --namespace "$NAMESPACE" \
    --values "$values" \
    $override_arg \
    $extra_args
}

chart_tgz=dependabot-gitlab-*.tgz

if [[ "$CI_JOB_NAME" == "upgrade"* ]]; then
  values_yml=".gitlab/ci/kube/values/${VALUES}.yaml"
  release_version_chart_dir="release/${CHART_DIR}"
  release_version_values_yml="release/${values_yml}"

  log_with_header "Run helm upgrade"

  title "Fetch previous release values.yml"
  helm repo add dependabot https://dependabot-gitlab.gitlab.io/chart
  release_version=$(helm search repo dependabot --output json | jq -r ".[-1].version")
  git fetch origin tag "v${release_version}" --no-tags
  git worktree add "release" "v${release_version}"

  title "Install current release"
  if [[ ! -f "$release_version_values_yml" ]]; then
    warn "Values file '${values_yml}' doesn't exist in release ${release_version}!"
    warn "Skipping running upgrade tests!"
    exit
  fi
  helm-install "install" "dependabot/dependabot-gitlab" "$release_version_values_yml"

  title "Upgrade dependabot release"
  helm-install "diff upgrade" $chart_tgz
  helm-install "upgrade" $chart_tgz
else
  log_with_header "Run helm install"
  helm-install "diff upgrade" $chart_tgz
  helm-install "install" $chart_tgz
fi
